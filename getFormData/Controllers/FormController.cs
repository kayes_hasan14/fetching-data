﻿using getFormData.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace getFormData.Controllers
{
    public class FormController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]

        public IActionResult Index(Form i)
        {
            Form obj = new Form();
            obj.name = i.name;
            obj.age = i.age;
            obj.address = i.address;
            obj.number = i.number; 

            return View(obj);
        }
    }
}
